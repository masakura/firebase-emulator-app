const functions = require('firebase-functions');

exports.environment = functions.https.onRequest((_, response) => {
    response.send({
        firestore: process.env.FIRESTORE_EMULATOR_HOST,
        auth: process.env.FIREBASE_AUTH_EMULATOR_HOST,
    });
});
